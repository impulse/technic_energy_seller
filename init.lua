

-- Copyright (C) 2023 Sandro del Toro

-- This file is part of Technic Energy Seller Minetest Mod.

-- Technic Energy Seller is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- Technic Energy Seller is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with Technic Energy Seller.  If not, see <https://www.gnu.org/licenses/>.



local modpath = core.get_modpath(core.get_current_modname())

dofile(modpath .. "/api.lua")
dofile(modpath .. "/hv_gen.lua")
dofile(modpath .. "/mv_gen.lua")
dofile(modpath .. "/lv_gen.lua")
