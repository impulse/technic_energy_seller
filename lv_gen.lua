

-- Copyright (C) 2023 Sandro del Toro

-- This file is part of Technic Energy Seller Minetest Mod.

-- Technic Energy Seller is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- Technic Energy Seller is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with Technic Energy Seller.  If not, see <https://www.gnu.org/licenses/>.


minetest.register_craft({
	output = 'technic:lv_energy_seller 1',
	recipe = {
		{'mcl_core:emerald', 'emeraldbank:bank', 'mcl_core:emerald'},
		{'technic:stainless_steel_ingot', 'technic:lv_transformer', 'technic:stainless_steel_ingot'},
		{'technic:stainless_steel_ingot', 'technic:lv_cable', 'technic:stainless_steel_ingot'},
	}
})

local texture_prefix = "technic_lv_grinder"

technic.register_energy_seller("lv_energy_seller", {
	tier="LV",
	supplied_max = 300000,
	tiles = {
		texture_prefix.."_top.png",
		texture_prefix.."_bottom.png",
		texture_prefix.."_side.png^mcl_core_emerald.png",
	},
})
