

-- Copyright (C) 2023 Sandro del Toro

-- This file is part of Technic Energy Seller Minetest Mod.

-- Technic Energy Seller is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- Technic Energy Seller is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with Technic Energy Seller.  If not, see <https://www.gnu.org/licenses/>.


local modname = core.get_current_modname()
local S = core.get_translator(modname)

function technic.register_energy_seller(name, data)
	local nodename = "technic:"..name
	local def = data
	assert(def.tier, "Technic register_energy_seller requires `tier` field")

	local tier = def.tier
	local supplied_max = def.supplied_max
	if not supplied_max or type(supplied_max) ~= "number" then
		supplied_max = 100000
	end
	local ltier = string.lower(tier)
	local infotext = S("Energy Seller @1 Generator", S(tier))

	local run = function(pos, node, run_state, network)
		local meta = minetest.get_meta(pos)
		local owner = meta:get_string("owner")
		local player = core.get_player_by_name(owner)
		local bankemeralds = emeraldbank.get_emeralds(owner)
		local supplied = meta:get_int("supplied")
		local charged = meta:get_int("charged")

		if player and bankemeralds >= 1 and network and network.demand then
			local charge_to_give = network.demand
			meta:set_string("infotext", S("@1 Active (@2)\nCharged to @3: @4 emeralds", infotext, technic.EU_string(charge_to_give), owner, charged))
			meta:set_int(tier.."_EU_supply", charge_to_give)
			meta:set_int("supplied", supplied + charge_to_give)
		else
			meta:set_string("infotext", S("@1 Idle\nOwner: @2", infotext, owner))
			meta:set_int(tier.."_EU_supply", 0)
		end
		if player and supplied > supplied_max then
			emeraldbank.add_emeralds(player, -1)
			meta:set_int("charged", charged + 1)
			meta:set_int("supplied", 0)
		end
	end

	def.tiles = def.tiles or {
		modname.."_"..name.."_top.png",
		modname.."_"..name.."_bottom.png",
		modname.."_"..name.."_side.png",
		modname.."_"..name.."_side.png",
		modname.."_"..name.."_side.png",
		modname.."_"..name.."_side.png"
	}
	def.groups = def.groups or {snappy=2, choppy=2, oddly_breakable_by_hand=2,
		technic_machine=1, ["technic_"..ltier]=1, pickaxey=2, handy=1}
	def._mcl_blast_resistance = 1
	def._mcl_hardness = 1.5
	def.connect_sides = def.connect_sides or {"bottom", "top"}
	def.sounds = def.sounds or mcl_sounds.node_sound_metal_defaults()
	def.description = def.description or S("Energy Seller @1 Generator", S(tier))
	def.active = def.active or false
	def.on_construct = def.on_construct or function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_int(tier.."_EU_supply", 0)
	end
	def.after_place_node = function(pos, placer, itemstack, pointed_thing)
		local meta = minetest.get_meta(pos)
		local name = placer:get_player_name()
		meta:set_string("owner", name)
	end
	def.technic_run = def.technic_run or run

	minetest.register_node(":"..nodename, def)
	technic.register_machine(tier, nodename, technic.producer)
end
