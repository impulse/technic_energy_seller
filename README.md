
# Technic Energy Seller (MineClone2/MineClonia)

[![ContentDB](https://content.minetest.net/packages/NathanielFreeman/technic_energy_seller/shields/downloads/)](https://content.minetest.net/packages/NathanielFreeman/technic_energy_seller/)

![Screenshot](screenshot.png)

Code under AGPLv3-or-later License

Technic Energy Seller Mod is [Libre/Free Software](https://www.gnu.org/philosophy/free-sw.html), copy and share.

[This is not open source](https://www.gnu.org/philosophy/open-source-misses-the-point.html)!


## Description

Buy energy in [technic mod](https://github.com/minetest-mods/technic) with emeralds from [emeraldbank](https://codeberg.org/usrib/emeraldbank) mod.

EU price depends on whether the generator is HV, MV or LV:

- HV: 1 emerald by 100kEU
- MV: 1 emerald by 200kEU
- LV: 1 emerald by 300kEU

The generator can supply infinite energy if you can pay it.


This mod is for [MineClone2](https://content.minetest.net/packages/Wuzzy/mineclone2/)/[MineClonia](https://codeberg.org/mineclonia/mineclonia) Games, dont work with MTG


Feel free to contribute, send MR/PR or issues.
